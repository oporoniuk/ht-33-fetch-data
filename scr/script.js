// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Це технологія , яка дозволяє надсилати та отримувати дані з вебсервера без оновлення сторінкию
// Щоб застосувати AJAX ми викликаємло обєкт XMLHttpRequest()

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:

// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films 
// та отримати список усіх фільмів серії Зоряні війни

// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
// Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
// Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, 
// вивести цю інформацію на екран під назвою фільму.


const API_URL = 'https://ajax.test-danit.com/api/swapi/films';
const filmList = document.getElementById('filmList');
const readyState = false;


function getFilmList() {
    return fetch(API_URL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())
        .catch((error) => console.log(error));

};

function getCharacter(url) {
    return fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json());

};

getFilmList().then(films => {
    films.forEach(film => {
        const filmItem = document.createElement('li');
        const filmCharacters = document.createElement('p');
        filmCharacters.classList = 'filmCharacters';

        filmItem.innerHTML = film.name + '<br>' +
            film.episodeId + '<br>' +
            film.openingCrawl + '<br>'

        const loader = document.createElement('span');
        loader.classList = 'loader';
        filmItem.append(loader);
        let loadingCounter = 0;

        function onCharacterLoaded() {
            loadingCounter--
            if (loadingCounter < 1) {
                loader.style.display = "none";
            }
            console.log(loadingCounter);
        };


        filmItem.append(filmCharacters);
        filmList.append(filmItem);
        console.log(filmItem);

        loadingCounter = loadingCounter + film.characters.length;
        film.characters.forEach((url) => {

            getCharacter(url).then(character => {
                filmCharacters.innerHTML = filmCharacters.innerHTML +
                    character.name + '<br>';
                onCharacterLoaded();
                console.log(character)
            });
        });
    });


});


// Необов'язкове завдання підвищеної складності

// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. 
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.


// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Література:

// Використання Fetch на MDN
// Fetch
// CSS анімація
// Події DOM
